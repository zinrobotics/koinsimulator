const db = require('../db') //this is required
const CryptoCurrency = require('../db/models/cryptocurrency');


const router = require('express').Router()

router.get('/', function(req, res, next) {
    CryptoCurrency.findAll({
            
        })
        .then(result => {
            res.status(200).send(result);
        })
        .catch(next);
});

router.get('/:id', function(req, res, next) {
    CryptoCurrency.findOne({
            where:{id:req.params.id},
        })
        .then(result => {
            res.status(200).send(result);
        })
        .catch(next);
});

module.exports = router
