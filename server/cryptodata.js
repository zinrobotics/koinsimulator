const db = require('../db') //this is required
const CryptoData = require('../db/models/cryptodata');


const router = require('express').Router()

router.get('/', function(req, res, next) {
    CryptoData.findAll({
        order: [['date', 'DESC']] 
            
        })
        .then(result => {
            res.status(200).send(result);
        })
        .catch(next);
});

router.get('/:id', function(req, res, next) {
    CryptoData.findOne({
            where:{id:req.params.id},
        })
        .then(result => {
            res.status(200).send(result);
        })
        .catch(next);
});

module.exports = router