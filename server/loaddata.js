const db = require('../db') //this is required
const CryptoData = require('../db/models/cryptodata');
const CryptoCurrency = require('../db/models/cryptocurrency');
const alpha = require('alphavantage')({key : 'VVRIG7EUXA81NI33'});

const apikey = 'VVRIG7EUXA81NI33';

const router = require('express').Router()




// This should load the data from API ensuring we don't duplicate data.
// Get Existing Data

router.get('/', function(req, res, next) { 

        CryptoCurrency.findAll({
                })
        .then(result => {
            result.forEach(currency => {
                console.log('handling : ' + currency.currency_code);
                alpha.crypto.daily(currency.currency_code,'usd')
                .then(data =>{
                    Object.keys(data["Time Series (Digital Currency Daily)"]).forEach(function(key) {
                        stockDate=data["Time Series (Digital Currency Daily)"][key];

                        var selector = { 
                            where : {
                                cryptocurrency_id : currency.id, 
                                date : key
                            }, 

                            defaults: {
                                cryptocurrency_id: currency.id, 
                                date: key, 
                                open: Number(stockDate["1b. open (USD)"]), 
                                high: Number(stockDate["2b. high (USD)"]),
                                low: Number(stockDate["3b. low (USD)"]),
                                close: Number(stockDate["4b. close (USD)"]),
                                volume: Number(stockDate["5. volume"]), 
                                market_cap:Number(stockDate["6. market cap (USD)"])
                            }
                        };


                        CryptoData.findOrCreate( selector ).spread((crypto, created) => {
                            console.log(crypto.get({
                                plain: true
                            }))
                            console.log(created)
                        });

                    });
                })
            });
            res.status(200).send(result);
        })
        .catch(next);
    });

module.exports = router