const api = module.exports = require('express').Router()
const products = require('./products');
const reviews = require('./reviews');
const cryptocurrencies = require('./cryptocurrencies');
const cryptodata = require('./cryptodata');
const loaddata = require('./loaddata');
const axios = require('axios');
const feed = require('./feed');

// import products from './products';
api
  .get('/express-test', (req, res) => res.send({express: 'working!'})) //demo route to prove api is working
  .use('/products', products)
  .use('/reviews', reviews)
  .use('/cryptocurrencies', cryptocurrencies)
  .use('/cryptodata', cryptodata)
  .use('/loaddata', loaddata)
  .use('/getFeed', feed)


// No routes matched? 404.
api.use((req, res) => res.status(404).end())
