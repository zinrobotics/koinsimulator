import React, { Component } from 'react';
import classnames from 'classnames';
import Chart from 'chart.js';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import {Scatter, Line} from 'react-chartjs-2';
import NewsAPI from '../../newsapi';


export default class KoinTable extends Component {
	constructor(props){
		super(props);
		this.state = {
            source: '',
			news : [],
		};
		this.getFeed = this.getFeed.bind(this);
		this.filterFeed = this.filterFeed.bind(this);
		// var url = '/api/getFeed/'+this.props.currency;
		// fetch(url).then(function(response) {
		//   	return response.json();
		//   }).then (feed => {
		//   	this.setState({
		//   		news : feed
		//   	});
		// });
	}

    filterFeed(feed) {
        const selectedDate = this.props.date;
        let filtered_feed = [];
        if (selectedDate) {
            feed.forEach(function (news) {
                const news_date = news.date.split('T')[0];
                if (news_date === selectedDate) {
                    filtered_feed.push(news)
                }
            });
		}
        return filtered_feed;

	}

	getFeed(selected_date, q){
		if (selected_date && q) {
            // const NewsAPI = require('newsapi');
            const newsapi = new NewsAPI('741e9fa21a804f838481a729ae183801');
            return newsapi.v2.everything({
                q: q.split(' ').join(''),
                // sources: 'google-news,bbc-news,financial-times,financial-post,australian-financial',
                sources: 'crypto-coins-news',
                from: selected_date,
                to: selected_date,
                language: 'en',
                sortBy: 'relevancy',
                // page: 2
            }, {'mode': 'no-cors'}).then(response => {
                if (response.articles.length){
                    const news = response.articles.map(function(article) {
                        article.table = <div data={article} cellPadding="2" cellSpacing="3">
							<div className="col-sm-2">
								<img src={article.urlToImage} style={{height:"80px",width:"105px", marginLeft:"-10px"}}/>
							</div>
							<div className="col-sm-10">
								<h4>{article.title}</h4>
								<a href={article.url}>{article.url}</a>
								<p>{article.description}</p>
							</div>

						</div>;
                        return article;
                    });
					this.setState({
						source: '',
						news: news
					});
				} else {
                    var url = '/api/getFeed/'+this.props.currency_name;
                    fetch(url).then(function(response) {
                      	return response.json();
                      }).then (feed => {
                      	const news = this.filterFeed(feed);
                      	if (news.length){
                            this.setState({
								source: 'rss',
                                news : news
                            });
						}
                      	else {
                            this.setState({
								source: '',
                                news: [{table: <tr><td>Sorry, there is no data available for selected date</td></tr>}]
                            });
                        }
                    });

				}

            })
        }
	}

	componentDidUpdate(prevProps, prevState){
		if (prevProps.currency_name !== this.props.currency_name || prevProps.date !== this.props.date) {
            this.setState({
                news : []
            });
            this.getFeed(this.props.date, this.props.currency_name);

            // var url = '/api/getFeed/'+this.props.currency;
			// console.log('receiving new props : ' + url);
			// fetch(url).then(function(response) {
			//   	return response.json()
			//   }).then (feed => {
			//   	this.setState({
			//   		news : feed
			//   	});
			// });
		}
	}


  	render() {

		  	 function linkFormat(cell, row) {
					if (cell !== undefined)
						return '';
					else {
						return '<a href="'+ cell +'"/>';
					}
			}

			function dateFormat(cell, row){
				if (cell===undefined) return '';

				return Date(cell) ;
			}

			function descriptionFormat(cell, row){
				if (cell===undefined) return '';
				return (cell);
			}

		  	if (this.state.news.length === 0) return 'Loading...';
			console.log('Here', this.state.news);
        	if (this.state.source === 'rss'){
        		return (
					<div>
						<BootstrapTable striped data={this.state.news} height='420' width='420' scrollTop={ 'Top' }>
							<TableHeaderColumn dataFormat={descriptionFormat} isKey dataField='description'>News Feed</TableHeaderColumn>
						</BootstrapTable>
					</div>
				)
			} else {
                return (

					<div>
						<BootstrapTable striped data={this.state.news} height='420' width='420' scrollTop={ 'Top' }>
							<TableHeaderColumn dataFormat={descriptionFormat} isKey dataField='table'>News Feed</TableHeaderColumn>
						</BootstrapTable>

					</div>)
			}
  }
}