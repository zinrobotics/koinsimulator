import React, {Component} from 'react';
import classnames from 'classnames';
import Chart from 'chart.js';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import {Scatter, Line} from 'react-chartjs-2';
import KoinFeed from './KoinFeed';
import './style.css';


var AmCharts = require('@amcharts/amcharts3-react');

export default class KoinTable extends Component {
    constructor(props) {
        super(props);
        this.onRowClick = this.onRowClick.bind(this);
        this.state = {
            data: [],
            processed_data: [],
            KoinTableStyle: {},
            FilteredNewsProps: {hidden: true},
            selectedDate: ''
        }

    }

    onRowClick(row) {
        const date = row.date;
        this.setState({
            KoinTableStyle: {
                float: 'left',
                width: '50%'
            },

            FilteredNewsProps: {
                hidden: false
            },
            selectedDate: date
        });
    }

    componentWillMount() {
        fetch('/api/cryptodata').then(function (response) {
            return response.json()
        }).then(data => {
            this.setState({
                data: data
            });
        });
    }

    // shouldComponentUpdate(nextProps, nextState){
        // if (this.state.data.length){
        //     debugger;
        //     let startDate = this.props.startDate.format("YYYY-MM-DD");
        //     let endDate = this.props.endDate.format("YYYY-MM-DD");
        //     if (startDate === nextProps.startDate.format("YYYY-MM-DD") &&
        //         endDate === nextProps.endDate.format("YYYY-MM-DD")){
        //         return false
        //     }
        // }
        // return this.state.data.length;
    // }

    render() {
        var currency_index = this.props.index;
        var startDate = this.props.startDate.format("YYYY-MM-DD");
        var endDate = this.props.endDate.format("YYYY-MM-DD");


        var table_data = this.state.data.filter(row => ((row.cryptocurrency_id === this.props.index)
            && (row.date >= startDate) && (row.date <= endDate) ));

        var processed_data = table_data.map((item, index) => {
            var processed_item;
            var next_item = table_data[index + 1];
            var previous_price, previous_volume, current_price, current_volume, abs_volume, abs_price, price_change,
                volume_change;

            current_price = item.close;
            current_volume = item.volume;

            if (next_item) {
                previous_price = next_item.close;
                previous_volume = next_item.volume;
                price_change = ((current_price - previous_price) / previous_price * 100).toFixed(2);
                volume_change = ((current_volume - previous_volume) / previous_volume * 100).toFixed(2);
                abs_price = (10 * Math.abs(price_change)).toFixed(2);
                abs_volume = (10 * Math.abs(volume_change)).toFixed(2);
            }

            processed_item = {
                date: item.date,
                current_price: current_price,
                current_volume: current_volume,
                previous_price: previous_price,
                previous_volume: previous_volume,
                price_change: price_change,
                volume_change: volume_change,
                abs_price: abs_price,
                abs_volume: abs_volume

            };

            return processed_item;
        });

        var candleStickData = table_data.map((item, index) => {
            var processed_item = {
                date: item.date,
                open: item.open,
                high: item.high,
                low: item.low,
                close: item.close,

            };
            return processed_item;
        });


        var price_data = processed_data.map(item => item.current_price);
        var abs_price_data = processed_data.map(item => item.abs_price);
        var volume_data = processed_data.map(item => item.current_volume);
        var abs_volume_data = processed_data.map(item => item.abs_volume);
        var dates_data = processed_data.map(item => item.date);

        var combined_pv = price_data.map((element, index) => {
            return {
                x: element,
                y: volume_data[index]
            }
        });


        var max_price = Math.max.apply(Math, processed_data.slice(0, processed_data.length - 1).map(function (item) {
            return Number(item.abs_price);
        }));
        var min_price = Math.min.apply(Math, processed_data.slice(0, processed_data.length - 1).map(function (item) {
            return item.abs_price;
        }));

        var max_volume = Math.max.apply(Math, processed_data.slice(0, processed_data.length - 1).map(function (item) {
            return item.abs_volume;
        }));
        var min_volume = Math.min.apply(Math, processed_data.slice(0, processed_data.length - 1).map(function (item) {
            return item.abs_volume;
        }));


        function priceFormatter(cell, row) {
            if (cell === undefined) return '';

            return '<i class="glyphicon glyphicon-usd"></i> ' + cell;
        }

        function colorPriceFormatter(cell, row) {
            if (cell === undefined) return '';

            var heat_index = (cell - min_price) / (max_price - min_price) * 1;
            return '<div style="background-color: rgba(255,0,0, ' + heat_index + ')" > ' + cell + '</div>';
        }

        function colorVolumeFormatter(cell, row) {
            if (cell === undefined) return '';


            var heat_index = (cell - min_volume) / (max_volume - min_volume) * 1;


            return '<div style="background-color: rgba(255,0,0, ' + heat_index + ')" > ' + cell + '</div>';
        }

        const priceVolumeData = {
            labels: ['Scatter'],
            datasets: [
                {
                    label: 'Actual Price vs Volume',
                    fill: true,
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 10,
                    data: combined_pv
                }
            ]
        };


        const absPriceAbsVolume = {
            labels: dates_data,
            datasets: [{
                label: 'Abs Price %',
                data: abs_price_data,
                fill: true,
                backgroundColor: 'rgba(75,192,192,0.4)',
                pointBorderColor: 'rgba(75,192,192,1)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 5,
                pointHitRadius: 10,
            },
                {
                    label: 'Abs Volume %',
                    data: abs_volume_data,
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 10,
                }]
        };

        const volumeData = {
            labels: dates_data,
            datasets: [{
                label: 'Volume versus Time',
                data: volume_data,
                backgroundColor: 'rgba(75,192,192,0.4)',
                pointBorderColor: 'rgba(75,192,192,1)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 5,
                pointHitRadius: 10,
            }]
        };

        const priceData = {
            labels: dates_data,
            datasets: [{
                label: 'Price versus Time',
                data: price_data,
                backgroundColor: 'rgba(75,192,192,0.4)',
                pointBorderColor: 'rgba(75,192,192,1)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 5,
                pointHitRadius: 10,
            }]
        };


        const options = {
            elements: {
                line: {
                    tension: 0, // disables bezier curves
                }
            },
            onRowClick: this.onRowClick
        }


        return (
            <div>
                <div className="row">
                    <div style={this.state.KoinTableStyle}>
                        <BootstrapTable striped data={processed_data} options={options} height='420'
                                        scrollTop={'Bottom'}>
                            <TableHeaderColumn isKey dataField='date'>Date</TableHeaderColumn>
                            <TableHeaderColumn dataFormat={priceFormatter}
                                               dataField='current_price'>Price</TableHeaderColumn>
                            <TableHeaderColumn dataField='current_volume'>Volume</TableHeaderColumn>
                            <TableHeaderColumn dataField='price_change'>P%</TableHeaderColumn>
                            <TableHeaderColumn dataFormat={colorPriceFormatter} dataField='abs_price'>Abs
                                P% </TableHeaderColumn>
                            <TableHeaderColumn dataField='volume_change'>V%</TableHeaderColumn>
                            <TableHeaderColumn dataFormat={colorVolumeFormatter} dataField='abs_volume'>Abs
                                V% </TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                    <div className="col-xs-6" style={{float: 'right'}} {...this.state.FilteredNewsProps}>
                        <KoinFeed currency={this.props.currency} currency_name={this.props.currency_name}
                                  index={this.props.index + 1} date={this.state.selectedDate}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6">
                        <Scatter data={priceVolumeData}/>
                    </div>
                    <div className="col-sm-6">
                        <Line data={absPriceAbsVolume} options={options}/>
                    </div>
                    <div className="col-sm-6">
                        <Line data={volumeData}/>
                    </div>
                    <div className="col-sm-6">
                        <Line data={priceData}/>
                    </div>
                    <div>
                        <AmCharts.React
                            style={{
                                width: "100%",
                                height: "500px"
                            }}
                            options={{
                                "type": "serial",
                                "theme": "light",
                                "dataDateFormat": "YYYY-MM-DD",
                                "valueAxes": [{
                                    "position": "left"
                                }],
                                "graphs": [{
                                    "id": "g1",
                                    "balloonText": "Open:<b>[[open]]</b><br>Low:<b>[[low]]</b><br>High:<b>[[high]]</b><br>Close:<b>[[close]]</b><br>",
                                    "closeField": "close",
                                    "fillColors": "#7f8da9",
                                    "highField": "high",
                                    "lineColor": "#7f8da9",
                                    "lineAlpha": 1,
                                    "lowField": "low",
                                    "fillAlphas": 0.9,
                                    "negativeFillColors": "#db4c3c",
                                    "negativeLineColor": "#db4c3c",
                                    "openField": "open",
                                    "title": "Price:",
                                    "type": "candlestick",
                                    "valueField": "close"
                                }],
                                "chartScrollbar": {
                                    "graph": "g1",
                                    "graphType": "line",
                                    "scrollbarHeight": 30
                                },
                                "chartCursor": {
                                    "valueLineEnabled": true,
                                    "valueLineBalloonEnabled": true
                                },
                                "categoryField": "date",


                                "dataProvider": candleStickData
                            }
                            }

                            listeners={[{
                                "event": "rendered",
                                "method": (e) => {
                                    console.log(e);
                                }
                            }]}

                        />
                    </div>


                </div>

            </div>
        );


    }
}