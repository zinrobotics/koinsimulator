import React, {Component} from 'react';

export default class Dropdown extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.currencies.length === 0){
            return <div className="form-group col-sm-4">
                <div>Loading Currencies...</div>
            </div>
        }
        else {
            let counter = 1;
            return <div className="form-group col-sm-4">
                <label>Select list (select one):</label>
                <select className='form-control' id="currency_selection" onChange={this.props.onChange}>
                    {this.props.currencies.map(currency => {
                        return <option value={currency.currency_code} key={counter++}>{currency.currency_name}</option>
                    })}
                </select>
            </div>
        }
    }
}