import React, { Component } from 'react';
import classnames from 'classnames';



export default class KoinChart extends Component {
	constructor(props){
		super(props);

		this.state = {
			data : [],
		}


	}

	componentWillMount(){ 
		
	}

	componentDidMount(){

		var currency_index = this.props.currency_index;
	
		var table_data = this.state.data.filter(row => row.cryptocurrency_id === currency_index );

		var processed_data = table_data.map((item, index) =>{
			var processed_item;
          	var next_item = table_data[index +1];
          	var previous_price, previous_volume, current_price, current_volume, abs_volume, abs_price, price_change, volume_change;
          	

          	if (next_item) {
          		current_price = item.close;
          		previous_price = next_item.close;
          		current_volume = item.volume;
          		previous_volume = next_item.volume;
          		price_change = ((current_price - previous_price)/previous_price * 100).toFixed(2);
          		volume_change = ((current_volume- previous_volume)/previous_volume * 100).toFixed(2);
          		abs_price = (10 * Math.abs(price_change)).toFixed(2);
          		abs_volume = (10 * Math.abs(volume_change)).toFixed(2);
          	}

          	processed_item = {
          		date : item.date,
          		current_price : current_price,
          		current_volume : current_volume,
          		previous_price : previous_price,
          		previous_volume : previous_volume,
          		price_change : price_change,
          		volume_change : volume_change,
          		abs_price : abs_price,
          		abs_volume : abs_volume

          	};
          	

          	return processed_item;
          });
	}

      

  render() {

  	if (this.state === null) return '<div> Loading... </div>';
  	
  	console.log(this.state.processed_data)
    return (
      <div className='row'>
	      <div className='col-sm-12'>
	        Currency : {this.props.currency}
	        <table className="table table-striped" data-show-header="true" data-pagination="true"
           data-id-field="name"
           data-page-list="[5, 10, 25, 50, 100, ALL]"
           data-page-size="5">
	            <thead>
	              <tr>
	                <th>
	                  Date
	                </th>
	                <th>
	                  Price
	                </th>
	                <th>
	                  Volume
	                </th>
	                <th>
	                  P%
	                </th>
	                <th>
	                  Abs P%
	                </th>
	                <th>
	                  V%
	                </th>
	                <th>
	                  Abs V%
	                </th>
	              </tr>
	           	</thead>
	         	<tbody>

	         	</tbody>      
	        </table>
        </div>
      </div>
    );
  }
}