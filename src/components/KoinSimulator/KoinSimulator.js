import React, {Component} from 'react';
import {DateRangePicker, isInclusivelyBeforeDay} from 'react-dates';
import Chart from 'chart.js';
import classnames from 'classnames';
import KoinChart from './KoinChart';
import KoinTable from './KoinTable';
import Dropdown from './_dropdown';
import moment from 'moment';



export default class KoinSimulator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedCrypto: 'BTC',
            selectedCurrency: 'Bit Coin',
            currency_index: 0,
            currencies: [],
            startDate: moment().startOf('month'),
            endDate: moment()
        };

        this.handleChange.bind(this);
    }
    
    componentWillMount() {
        fetch('/api/cryptocurrencies').then(function (response) {
            return response.json()
        }).then(data => {
            this.setState({
                currencies: data
            });
        });
    }

    handleChange(e) {
        let index = document.getElementById('currency_selection').selectedIndex;
        console.log(index);
        this.setState({
            selectedCrypto: this.state.currencies[index].currency_code,
            selectedCurrency: this.state.currencies[index].currency_name,
            currency_index: document.getElementById('currency_selection').selectedIndex
        });
    }

    render() {
        return (
            <div>
                <form>
                    <div className="row">
                        <Dropdown currencies={this.state.currencies} onChange={this.handleChange.bind(this)}/>
                        <div className='col-sm-4'/>
                        <div className='col-sm-4'>
                            <DateRangePicker
                                startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                                endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                                onDatesChange={({startDate, endDate}) => this.setState({
                                    startDate,
                                    endDate
                                })} // PropTypes.func.isRequired,
                                focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                onFocusChange={focusedInput => this.setState({focusedInput})}// PropTypes.func.isRequired,
                                isOutsideRange={day => !isInclusivelyBeforeDay(day, moment())}// Disable future dates
                            />
                        </div>
                    </div>
                </form>
                <KoinTable currency={this.state.selectedCrypto} currency_name={this.state.selectedCurrency}
                           index={this.state.currency_index + 1} startDate={this.state.startDate}
                           endDate={this.state.endDate}/>
            </div>
        );
    }
}