import React from 'react';
import Header from '../Header/Header';

class GuestGreeting extends React.Component {
    render() {
        return <div className='container-fluid'>
            <Header button={this.props.button}/>
        </div>;
    }
}

export default GuestGreeting;
