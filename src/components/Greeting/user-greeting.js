import React from 'react';
import Header from '../Header/Header';
import KoinSimulator from '../KoinSimulator/KoinSimulator';

class UserGreeting extends React.Component {

    render() {
        return <div className='container-fluid'>
            <Header button={this.props.button}/>
            <KoinSimulator/>
        </div>;
    }
}

export default UserGreeting;
