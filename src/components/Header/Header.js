import React from 'react';


class Header extends React.Component {

    render() {
        return <div className="jumbotron text-center">
            <h1>Koin Simulator</h1>
            <p>Crypto Currency Analysis & Simulator</p>
            {this.props.button}
        </div>


    }
}

export default Header;
