import React from 'react';
import GoogleLogin from 'react-google-login';
import { GoogleLogout } from 'react-google-login';
import Greeting from '../Greeting/app-greeting';

class GoogleAuth extends React.Component{
    constructor(props) {
        super(props);
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
        this.responseGoogleFailure = this.responseGoogleFailure.bind(this);
        this.state = {isLoggedIn: false};
    }
    responseGoogleFailure(response) {
        console.log('Callback');
        console.log(response);
        this.setState({
            isLoggedIn: false
        })
    }

    login(response){
        console.log('Logged In');
        console.log(response.profileObj);
        this.setState({
            isLoggedIn: true
        })

    }
    logout(response){
        console.log('Logged Out');
        this.setState({
            isLoggedIn: false
        })
    }

    render(){
        const isLoggedIn = this.state.isLoggedIn;
        let button = null;
        if (isLoggedIn) {
            button = <GoogleLogout buttonText="Logout" onLogoutSuccess={this.logout}/>;
        } else {
            button = <GoogleLogin
                clientId="216851034999-e7jq6hc2jk37snu4bs30rdkfe629r2ke.apps.googleusercontent.com"
                buttonText="Login"
                onSuccess={this.login}
                onFailure={this.responseGoogleFailure}/>;
        }
        return <Greeting isLoggedIn={isLoggedIn} button={button} />
    }
}


export default GoogleAuth;