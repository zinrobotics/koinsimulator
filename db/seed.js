const db = require('../db')

const seedCryptoCurrencies = () => db.Promise.map([
 {'currency_name': 'Bit Coin', 'currency_code': 'BTC'},
 {'currency_name': 'Ethereum', 'currency_code': 'ETH'},
 {'currency_name': 'Dash', 'currency_code': 'DASH'},
 {'currency_name': 'Ripples', 'currency_code': 'XRP'}
 ], cryptocurrency => db.model('cryptocurrency').create(cryptocurrency));



 db.didSync
   .then(() => db.sync({force: true}))
   .then(seedCryptoCurrencies)
   .then(cryptocurrencies => console.log(`Seeded ${cryptocurrencies.length} cryptocurrencies OK`))
   .catch(error => console.error(error))
   .finally(() => db.close())
