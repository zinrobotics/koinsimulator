'use strict';

const Product = require('./product')
const Review = require('./review');
const CryptoCurrency = require('./cryptocurrency');
const CryptoData = require('./cryptodata')

Product.hasMany(Review);
Review.belongsTo(Product);

CryptoCurrency.hasMany(CryptoData);
CryptoData.belongsTo(CryptoCurrency);


module.exports = {Product, Review, CryptoData,CryptoCurrency};
