'use strict'

const Sequelize = require('sequelize');
const db = require('../index.js');

const CryptoData = db.define('cryptodata', {
  date: {
  	type: Sequelize.DATEONLY,
  	allowNull: false
  },
  
  open: {
  	type: Sequelize.INTEGER,
  	allowNull: false
  },
 
  high: {
  	type: Sequelize.INTEGER,
  	allowNull: false
  },

  
  low: {
  	type: Sequelize.INTEGER,
  	allowNull: false
  },
  
  close: {
  	type: Sequelize.INTEGER,
  	allowNull: false
  },
  
  volume: {
  	type: Sequelize.INTEGER,
  	allowNull: false
  },
  
  market_cap: {
  	type: Sequelize.INTEGER,
  	allowNull: false
  }

});

module.exports = CryptoData;
