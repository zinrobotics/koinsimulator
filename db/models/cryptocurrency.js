'use strict'

const Sequelize = require('sequelize');
const db = require('../index.js');

const CryptoCurrency = db.define('cryptocurrency', {
  currency_name: {
  	type: Sequelize.STRING,
  	allowNull: false
  },
  
  currency_code: {
  	type: Sequelize.STRING,
  	allowNull: false
  }

});

module.exports = CryptoCurrency;
